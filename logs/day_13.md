## 内容
- Jpa的映射关系（one to one ,one to many, many to one ,many to many）
- FetchType属性详解
- CascadeType属性详解、例子分析
- jpa测试用例分析
- 根据Repo接口的方法名构造查询
- 构造查询的返回类型
- 利用lombok的@Value，构造返回类型DTO类
- 利用泛型，动态构造查询返回类型。

## 作业
分小组完成。模拟一个业务场景（如，选课、用户登录），设计数据库表，建立相关Entity实体，画出E-R图，并写测试用例，说明CascadeType各个属性的作用。